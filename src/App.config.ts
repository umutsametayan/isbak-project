import { ITrafficLightConfig, ITrafficLight } from './App.types';

export const config = [
    { id: 'group1', colors: ['red', 'yellow', 'green'] },
    { id: 'group2', colors: ['red', 'yellow', 'green'] },
    { id: 'pedestrian', colors: ['red', 'yellow', 'green'] },
] as ITrafficLightConfig[];

export const initialState: ITrafficLight = {
    group1: 'red',
    group2: 'red',
    pedestrian: 'red',
};

export const steps: ITrafficLight[] = [
    { group1: 'red', group2: 'red', pedestrian: 'red' },
    { group1: 'green', group2: 'red', pedestrian: 'red' },
    { group1: 'yellow', group2: 'red', pedestrian: 'red' },
    { group1: 'red', group2: 'green', pedestrian: 'red' },
    { group1: 'red', group2: 'yellow', pedestrian: 'red' },
    { group1: 'red', group2: 'red', pedestrian: 'red' },
];
