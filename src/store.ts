// src/store/store.ts
import { configureStore } from '@reduxjs/toolkit';
import { postsApi } from "./posts/postsapi"

export const store = configureStore({
  reducer: {
    // Setup the API reducer
    [postsApi.reducerPath]: postsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(postsApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
