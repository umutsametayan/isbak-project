import React from 'react';
import { useGetPostByIdQuery } from './postsapi';
import { IPostDetailsProps } from './post.types'


const PostDetails: React.FC<IPostDetailsProps> = ({ postId }) => {
  const { data: post, error, isLoading } = useGetPostByIdQuery(postId);

  if (isLoading) return <span className="loader"></span>;
  if (error) return <div>Error occurred</div>;
  if (!post) return <div>No post found</div>;

  return (
    <div className='centered'>
      <button className='stage-btn' onClick = {() => window.location.reload()}>Başa Dön</button>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
    </div>
  );
};

export default PostDetails;
