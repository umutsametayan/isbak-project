export interface IPostDetailsProps {
    postId: number;
  }
  
export interface IPost {
    userId: number;
    id: number;
    title: string;
    body: string;
  }
  
export interface IPostsListProps {
    setCaseStatus: (status: boolean) => void;
    caseStatus: boolean;
  }
