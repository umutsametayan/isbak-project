import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

const postsApi = createApi({
  reducerPath: 'postsApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://jsonplaceholder.typicode.com/' }),
  endpoints: (builder) => ({
    getPosts: builder.query<Post[], void>({
      query: () => 'posts'
    }),
    getPostById: builder.query<Post, number>({
      query: (id) => `posts/${id}`
    }),
  }),
});

export const { useGetPostsQuery, useGetPostByIdQuery } = postsApi;
export { postsApi }; 
